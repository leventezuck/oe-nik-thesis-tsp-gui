function plot(data) {
    let trace = {
        x: $.map(data, function (city) {
            return city.x;
        }),
        y: $.map(data, function (city) {
            return city.y;
        }),
        mode: 'lines+markers',
        type: 'scatter'
    };

    let layout = {
        margin: {t: 0},
        hovermode: false,
        xaxis: {
            nticks: 10,
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: false
        },
        yaxis: {
            scaleanchor: "x",
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: false
        }
    };

    let config = {
        responsive: true,
        displayModeBar: false
    }

    Plotly.newPlot('graph-container', [trace], layout, config);
}
